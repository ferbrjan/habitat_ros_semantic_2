## Running AIHabitat

*To better understand what AIHabitat is and how to use it, I suggest watching the recorded tutorials from ECCV2020: https://aihabitat.org/tutorial/2020/*

To view or further use the model, one must first install a software to which allows an efficient exploration of the model. In our case AIHabitat:


Therefore, step 1 is to **install AIHabitat: habitat-sim and habitat-lab**

https://github.com/facebookresearch/habitat-lab#installation

I recommend to verify installation by running provided examples before moving further.



----

Once the installation is complete, you need to setup the **spring_simulation** package.

This can be done by

`cd spring_simulation`

`python setup.py develop`



---

## Visiting Broca model in AIHabitat



It is possible to have a virtual visit inside the models. Run 

```bash
<PATH_TO_spring_simulation>$ python generate_dataset.py
```

to  take a tour.  

**Moving around**

You can move there by 

| key  | action       |
| ---- | ------------ |
| W    | Move Forward |
| A    | Turn Left    |
| D    | Turn Right   |
| U    | Look Up      |
| J    | Look Down    |
| F    | Quit         |

 The movement is limited to 500 moves where the simulation terminates. To exit earlier press **f**.

---

**Model Selection**

![](./media/floorplan_broca_daycare.png)

![](./media/floorplan_living_lab.png)

It is possible to visit different model. This can be done by specifying ```--location (-l)``` flag. Use ``` -h``` to list them. 

for example:


```bash
<PATH_TO_spring_simulation>$ python generate_dataset.py -l livinglab_1 
```

will  take you to a tour inside Broca's Living Lab. there are currently four models:

| Model                               | Flag              |                              |
| ----------------------------------- | ----------------- | ---------------------------- |
| Daycare Hospital *with* Curtains    | ```hospital_1```  | ![](./media/hospital_1.png)  |
| Daycare Hospital *without* Curtains | ```hospital_2```  | ![](./media/hospital_2.png)  |
| Living Lab *with* Curtains          | ```livinglab_1``` | ![](./media/livinglab_1.png) |
| Living Lab *without* Curtains       | ```livinglab_2``` | ![](./media/livinglab_2.png) |

## Under the hood

Feel free to play around with  the ```visit.py``` script which provides the simplest possible interaction. After the environment is set, it loops in cycle of:

1. waiting for action

2. sending the action to the Habitat simulator

3. displaying the observations of predefined sensors

   | Daycare Hospital                                             | Living Lab                                                   |
   | ------------------------------------------------------------ | ------------------------------------------------------------ |
   | ![](./media/hospital_2.gif) | ![](./media/livinglab_1.gif) |

   



You may want to add any form of recording or manipulate the environment with object insertion/movement.  

There is a whole set of insert-able objects packed in data/objects which you can play with.

## **Media**

The ```media``` folder contains a short animations of a walk-trough, floor plans and few other images.

## Raw Models

I've removed the raw models to reduce size of repository. If you are interested in raw data please send me an email: [stanislav.steidl@cvut.cz](mailto:stanislav.steidl@cvut.cz)