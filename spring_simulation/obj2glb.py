import os


#Change here to Living_Lab if needed
orig_path = "data/objects_categorized/Hospital_dynamic"

orig_dirs = os.listdir(orig_path)
for file in orig_dirs:
    if not file.startswith('.'):
        path = orig_path + "/" + file
        obj_path = orig_path + "/" + file + "/" + file + "_obj"
        #print (path)
        dirs = os.listdir(obj_path)
        for item in dirs:
            if item.endswith(".obj"):
                final_path = obj_path + "/" + item
                final_path_glb = path + "/" + item[:-4] + ".glb"
                final_path_json = path + "/" + item[:-4] + ".object_config.json"
                print(final_path)
                print(final_path_glb)
                print(final_path_json)

                item_glb = item[:-4] + ".glb"

                command = "obj2gltf -i " + final_path + " -o " + final_path_glb
                stream = os.popen('echo Returned output')
                os.system(command)
                output = stream.read()
                output

                f = open(final_path_json, "w")
                f.write("{")
                f.write("\n")
                new_line = "    \"render_asset\": \"" + item_glb + "\""
                f.write(new_line)
                f.write("\n")
                f.write("}")
                f.close()
                print("file complete")


